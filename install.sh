#!/bin/sh

if [ -e ~/.dotfiles ]; then
    mv -f ~/.dotfiles.bk
fi

git clone git@bitbucket.org:yucchiy/dotfiles.git ~/.dotfiles

cd ~/.dotfiles

# remove previous dotfiles
for file in `cat dotfiles`
do
    echo "rm -rf ~/.${file}"
    mv -rf ~/.${file}.bk
done

git submodule init
git submodule update

# make symbolic links
for file in `cat dotfiles`
do
    echo "Processing $file ..... , please wait."
    ln -s ~/.dotfiles/${file} ~/.${file}
    echo "$file was done!"
done

# for vim
if [ -d ~/.vimbackup ]; then
    rm -rf ~/.vimbackup
fi
mkdir ~/.vimbackup

# install vundle
vim -u ~/.vim/vundle.vim -c ':BundleInstall!' -c ':q!' -c ':q!'
if [ `uname` = "Darwin" ]; then
    pushd ~/.dotfiles/vim/bundle/vimproc
    make -f make_mac.mak
    popd
fi

