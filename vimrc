" Essential {{{

augroup MyVimrc
  autocmd!
augroup END

" Encoding
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8,cp932,euc-jp
set fileformats=unix,dos,mac

" This is vim, not vi.
set nocompatible

" User-defined prefix
let mapleader = ','

" Display line number
set number

" No backup!
set nobackup

" Language settings
language message C
language time C

" Use twice the width of ASCII characters.
set ambiwidth=double

" When a file hasbeen detected to have benn changed, automatically read it again.
set autoread

" 編集履歴を保存して終了する
if has('persistent_undo') && isdirectory($HOME.'/.vim/undo')
    set undodir=~/.vim/undo
    set undofile
endif

" }}}

" NeoBundle {{{

if ! isdirectory(expand('~/.vim/bundle'))
    echoerr '~/.vim/bundle is not found!'
    finish
endif

filetype off
filetype plugin indent off

if has('vim_starting')
    set rtp+=~/.vim/bundle/neobundle.vim/
    call neobundle#rc(expand('~/.vim/bundle'))
endif

NeoBundle 'Shougo/neobundle.vim'
NeoBundle 'Shougo/vimproc',  {
            \ 'build' : {
            \       'windows' : 'echo "Please build vimproc manually."', 
            \       'cygwin'  : 'make -f make_cygwin.mak', 
            \       'mac'     : 'make -f make_mac.mak', 
            \       'unix'    : 'make -f make_unix.mak', 
            \   }
            \ }
NeoBundle 'Shougo/neocomplcache'

NeoBundle 'Shougo/unite-outline'
NeoBundle 'tsukkee/unite-help'

NeoBundle 'thinca/vim-quickrun'
NeoBundle 'thinca/vim-ref'
NeoBundle 'scrooloose/nerdtree'

NeoBundle 'hail2u/vim-css3-syntax'
NeoBundle 'tpope/vim-surround'

NeoBundle 'taichouchou2/surround.vim'
NeoBundle 'open-browser.vim'
NeoBundle 'mattn/webapi-vim'
NeoBundle 'tell-k/vim-browsereload-mac'
NeoBundle 'hail2u/vim-css3-syntax'
NeoBundle 'sass-compile.vim'
NeoBundle 'taichouchou2/html5.vim'
NeoBundle 'taichouchou2/vim-javascript'
NeoBundle 'kchmck/vim-coffee-script'

NeoBundle "osyo-manga/vim-precious"
NeoBundle "Shougo/context_filetype.vim"
NeoBundle "kana/vim-textobj-user"

NeoBundle 'glidenote/memolist.vim'

NeoBundleLazy 'Shougo/unite.vim',  {
                \ 'autoload' : {
                \       'commands' : ['Unite',  'UniteWithBufferDir', 
                \       'UniteWithCursorWord',  'UniteWithInput'], 
                \       'functions' : 'unite#start'
                \   }
                \ }

NeoBundleLazy 'Shougo/vimfiler', {
            \ 'autoload' : {
            \     'commands' : ['VimFiler', 'VimFilerCurrentDir',
            \                   'VimFilerBufferDir', 'VimFilerSplit',
            \                   'VimFilerExplorer']
            \     }
            \ }

NeoBundleLazy 'Shougo/neosnippet', {
      \ 'autoload' : {
      \   'commands' : ['NeoSnippetEdit', 'NeoSnippetSource'],
      \   'filetypes' : 'snippet',
      \   'insert' : 1,
      \   'unite_sources' : ['snippet', 'neosnippet/user', 'neosnippet/runtime'],
      \ }}

NeoBundleLazy 'alpaca-tc/vim-endwise.git', {
      \ 'autoload' : {
      \   'insert' : 1,
      \ }}

NeoBundleLazy 'taka84u9/vim-ref-ri', {
      \ 'depends': ['Shougo/unite.vim', 'thinca/vim-ref'],
      \ 'autoload': { 
      \   'filetypes': ['haml', 'ruby', 'eruby']
      \ }}

NeoBundleLazy 'tpope/vim-dispatch', { 'autoload' : {
      \ 'commands': ['Dispatch', 'FocusDispatch', 'Start']
      \ }}


NeoBundleLazy 'alpaca-tc/neorspec.vim', {
      \ 'depends' : ['alpaca-tc/vim-rails', 'tpope/vim-dispatch'],
      \ 'autoload': {
      \   'commands': ['RSpec', 'RSpecAll', 'RSpecCurrent', 'RSpecNearest', 'RSpecRetry']
      \ }}

NeoBundleLazy 'taichouchou2/rspec.vim', { 
    \ 'depends' : 'tpope/vim-rails',
    \ 'autoload': {
    \   'commands': ['RSpecAll', 'RSpecNearest', 'RSpecRetry', 'RSpecCurrent', 'RSpec']
    \ }}

NeoBundle 'nathanaelkane/vim-indent-guides'
NeoBundle 'matchit.vim'
NeoBundle 'markwu/vim-laravel4-snippets'
NeoBundle 'xsbeats/vim-blade'
" {{{ colorscheme
NeoBundle 'jpo/vim-railscasts-theme'
NeoBundle 'twilight'
NeoBundle 'w0ng/vim-hybrid'
NeoBundle 'molokai'
NeoBundle 'ciaranm/inkpot'
NeoBundle 'wombat256.vim'
NeoBundle 'jonathanfilip/vim-lucius'
NeoBundle 'pyte'
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'endel/vim-github-colorscheme'
NeoBundle 'aereal/vim-magica-colors'
NeoBundle 'ujihisa/unite-colorscheme'
" }}}

" {{{ lightline
NeoBundle 'itchyny/lightline.vim.git'
" }}}

" 書き込み権限の無いファイルを編集しようとした時
NeoBundleLazy 'sudo.vim'
" ReadOnly のファイルを編集しようとしたときに sudo.vim を遅延読み込み
autocmd MyVimrc FileChangedRO * NeoBundleSource sudo.vim
autocmd MyVimrc FileChangedRO * execute "command! W SudoWrite" expand('%')
" }}}

" Editing {{{

" Indent settings
"set autoindent
set tabstop=4 shiftwidth=4 softtabstop=4
set shiftround
set smarttab
set cindent
set expandtab
set wrap
set winwidth=30
set showmatch
set ruler
set scrolloff=4

" Search settings
set ignorecase
set smartcase
set hlsearch

" Showing settings
set list
set listchars=tab:>.,trail:_,extends:>,precedes:<
set shortmess& shortmess+=I
 set linespace=2

" IME Setting
set iminsert=0 imsearch=0

" Comment insert settings
set formatoptions-=r
set formatoptions-=o

" Editing settings
set noshowcmd
set lazyredraw
set ttyfast

" Folding settings
set foldenable
set foldmethod=marker

" GUI
set laststatus=2
" Use lightline
let g:lightline = {
      \ 'colorscheme': 'wombat'
      \ }

if has('gui_macvim')
    set t_Co=256
    set showtabline=2
    set imdisable
    set antialias
    set transparency=0
    set guifont=Ricty:h14
    "set background=light
    colorscheme lucius
    set background=dark
    "colorscheme hybrid
    set transparency=5
else
    set background=dark
    colorscheme lucius
    hi IndentGuidesOdd  ctermbg=black
    hi IndentGuidesEven ctermbg=darkgrey

    "hi IndentGuidesOdd  ctermbg=white
    "hi IndentGuidesEven ctermbg=lightgrey
endif

" Vim外にいるときに透明度をぐっと落とす
augroup hack234
    autocmd!
    if has('gui_macvim')
        autocmd FocusGained * set transparency=5
        autocmd FocusLost * set transparency=50
    endif
augroup END

set cursorline

set clipboard=unnamed

" }}}

filetype plugin indent on

" Unite {{{

let g:unite_enable_start_insert = 1

let g:unite_source_file_mru_filename_format = ''

let g:unite_source_file_mru_limit = 100

" Unite起動時のウィンドウ分割
let g:unite_split_rule = 'rightbelow'

" 使わないデフォルト Unite ソースをロードしない
let g:loaded_unite_source_bookmark = 1
let g:loaded_unite_source_tab = 1
let g:loaded_unite_source_window = 1
" unite-grep で使うコマンド
let g:unite_source_grep_default_opts = "-Hn --color=never"
" the silver searcher を unite-grep のバックエンドにする
if executable('ag')
    let g:unite_source_grep_command = 'ag'
    let g:unite_source_grep_default_opts = '--nocolor --nogroup --column' let g:unite_source_grep_recursive_opt = ''
endif

" Uniteで読み込まないパータンの追加
call unite#custom_source('file_rec', 'ignore_pattern', '\.png$\|\.jpg$\|\.jpeg$\|\.gif$\|\.mid$\|\.ttf$\|\.mp3$\|\~$')

" Unite custom actions {{{
"
" git のルートディレクトリを返す {{{
function! s:git_root_dir()
    if(system('git rev-parse --is-inside-work-tree') ==# "true\n")
        return system('git rev-parse --show-cdup')
    else
        echoerr 'current directory is outside git workingtree'
    endif
endfunction
" }}}

function! s:define_unite_actions()

    " Git リポジトリのすべてのファイルを開くアクション {{{
    let git_repo = { 'description' : 'all file in git repository' }
    function! git_repo.func(candidate)
        if(system('git rev-parse --is-inside-work-tree') ==# "true\n" )
            execute 'args'
                \ join( filter(split(system('git ls-files `git rev-parse --show-cdup`'),  '\n')
                \ ,  'empty(v:val) || isdirectory(v:val) || filereadable(v:val)') )
        else
            echoerr 'Not a git repository!'
        endif
    endfunction

    call unite#custom_action('file',  'git_repo_files',  git_repo)
    " }}}

    " ファイルなら開き，ディレクトリなら VimFiler に渡す {{{
    let open_or_vimfiler = {
        \ 'description' : 'open a file or open a directory with vimfiler',
        \ 'is_selectable' : 1,
        \ }

    function! open_or_vimfiler.func(candidates)
        for candidate in a:candidates
            if candidate.kind ==# 'directory'
                execute 'VimFiler' candidate.action__path
                return
            endif
        endfor
        execute 'args' join(map(a:candidates,  'v:val.action__path'),  ' ')
    endfunction

    call unite#custom_action('file',  'open_or_vimfiler',  open_or_vimfiler)
    "}}}

    " Finder for Mac 
    if has('mac')
        let finder = { 'description' : 'open with Finder.app' }
        function! finder.func(candidate)
            if a:candidate.kind ==# 'directory'
                call system('open -a Finder '.a:candidate.action__path)
            endif
        endfunction
        call unite#custom_action('directory',  'finder',  finder)
    endif

    " load once
    autocmd! UniteCustomActions
endfunction

" カスタムアクションを遅延定義
"augroup UniteCustomActions
"   autocmd!
"   autocmd FileType unite, vimfiler call <SID>define_unite_actions()
"augroup END

"C-gでいつでもバッファを閉じられる（絞り込み欄が空の時はC-hでもOK）
autocmd MyVimrc FileType unite imap <buffer><C-g> <Plug>(unite_exit)
autocmd MyVimrc FileType unite nmap <buffer><C-g> <Plug>(unite_exit)
""直前のパス削除
autocmd MyVimrc FileType unite imap <buffer><C-w> <Plug>(unite_delete_backward_path)
autocmd MyVimrc FileType unite nmap <buffer>h <Plug>(unite_delete_backward_path)
"ファイル上にカーソルがある時，pでプレビューを見る
autocmd MyVimrc FileType unite inoremap <buffer><expr>p unite#smart_map("p",unite#do_action('preview'))
""C-xでクイックマッチ
autocmd MyVimrc FileType unite imap <buffer><C-x> <Plug>(unite_quick_match_default_action)
"lでデフォルトアクションを実行
autocmd MyVimrc FileType unite nmap <buffer>l <Plug>(unite_do_default_action)
autocmd MyVimrc FileType unite imap <buffer><expr>l unite#smart_map("l", unite#do_action(unite#get_current_unite().context.default_action))
""jjで待ち時間が発生しないようにしていると候補が見えなくなるので対処
autocmd MyVimrc FileType unite imap <buffer><silent>jj <Plug>(unite_insert_leave)
" s を wincmd リマップする
autocmd MyVimrc FileType unite nmap <buffer>s <C-w>

nnoremap [unite] <Nop>
nmap     <Leader>u [unite]
" コマンドラインウィンドウで Unite コマンドを入力
nnoremap [unite]u                 :<C-u>Unite source<CR>
" バッファを開いた時のパスを起点としたファイル検索
nnoremap <silent>[unite]<Space>   :<C-u>UniteWithBufferDir -buffer-name=files file -vertical<CR>

"バッファ一覧
nnoremap <silent>[unite]b         :<C-u>Unite -quick-match -immediately -no-empty -auto-preview buffer<CR>

" プログラミングにおけるアウトラインの表示
nnoremap <silent>[unite]o         :<C-u>Unite outline -vertical -no-start-insert<CR>

" grep検索
nnoremap <silent>[unite]g         :<C-u>Unite -no-start-insert grep<CR>

" Uniteバッファの復元
nnoremap <silent>[unite]r         :<C-u>UniteResume<CR>

" Unite ソース一覧
nnoremap <silent>[unite]s         :<C-u>Unite source -vertical<CR>

" NeoBundle
nnoremap <silent>[unite]nb      :<C-u>AutoNeoBundleTimestamp<CR>:Unite neobundle/update -auto-quit<CR>
nnoremap <silent>[unite]nb        :<C-u>Unite neobundle/update:all -auto-quit -keep-focus<CR>

" Help
" help(項目が多いので，検索語を入力してから絞り込む)
nnoremap <silent>[unite]hh        :<C-u>UniteWithInput help -vertical<CR>

" Git のルートディレクトリを開く
"nnoremap <silent><expr>[unite]g  ":\<C-u>Unite file_rec:! -input=".fnamemodify(<SID>git_root_dir(), ":p")

" プロジェクトのファイル一覧
nnoremap <silent>[unite]p         :<C-u>Unite file_rec:! file/new<CR>

" タブ一覧
nnoremap <silent>[unite]t         :<C-u>Unite tab<CR>

" unite-colorscheme
nnoremap <silent>[unite]cs         :<C-u>Unite colorscheme -auto-preview<CR>

" }}}

" }}}

" NeoComplecache {{{

" 有効化
let g:neocomplcache_enable_at_startup = 1

" AutoComplPopを無効にする
let g:acp_enableAtStartup = 0

let g:neocomplecache_enable_smart_case = 1

let g:neocomplcache_enable_underbar_completion = 1

" シンタックスをキャッシュするときの最小文字長を5に
let g:neocomplcache_min_syntax_length = 5

" リスト表示
let g:neocomplcache_max_list = 300
let g:neocomplcache_max_keyword_width = 20

" Enable omni completion. {{{
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
" }}}

" NeoComplecache mapping {{{
inoremap <expr><C-g> neocomplcache#undo_completion()
inoremap <expr><C-s> neocomplcache#complete_common_string()
" <CR>: close popup and save indent.
" " <TAB>: completion
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
" "<C-h>, <BS>: close popup and delete backword char.
                      " inoremap <expr><C-h> neocomplcache#smart_close_popup()."\<C-h>"
                        " inoremap <expr><BS>neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><C-y> neocomplcache#close_popup()
" }}}

" }}}

" {{{ NeoSnipeets
if !exists("g:neosnippet#snippets_directory")
    let g:neosnippet#snippets_directory=""
endif
let g:neosnippet#snippets_directory=$HOME.'/.vim/snippets'

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable() <Bar><bar> neosnippet#jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable() <Bar><bar> neosnippet#jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif
" }}}

" Vimfiler {{{

" VimFiler の読み込みを遅延しつつデフォルトのファイラに設定 {{{
augroup LoadVimFiler
    autocmd!
    autocmd MyVimrc BufEnter,BufCreate,BufWinEnter * call <SID>load_vimfiler(expand('<amatch>'))
augroup END

" :edit {dir} や unite.vim などでディレクトリを開こうとした場合
function! s:load_vimfiler(path)
    if exists('g:loaded_vimfiler')
        autocmd! LoadVimFiler
        return
    endif

    let path = a:path
    " for ':edit ~'
    if fnamemodify(path, ':t') ==# '~'
        let path = expand('~')
    endif

    if isdirectory(path)
        NeoBundleSource vimfiler
    endif

    autocmd! LoadVimFiler
endfunction

" 起動時にディレクトリを指定した場合
for arg in argv()
    if isdirectory(getcwd().'/'.arg)
        NeoBundleSource vimfiler
        autocmd! LoadVimFiler
        break
    endif
endfor
"}}}

let g:vimfiler_as_default_explorer = 1
let g:vimfiler_safe_mode_by_default = 0
let g:vimfiler_enable_auto_cd = 1
let g:vimfiler_split_command = 'vertical rightbelow vsplit'
let g:vimfiler_execute_file_list = { 'rb' : 'ruby', 'pl' : 'perl', 'py' : 'python' ,
            \ 'txt' : 'vim', 'vim' : 'vim' , 'c' : 'vim', 'h' : 'vim', 'cpp' : 'vim',
            \ 'hpp' : 'vim', 'cc' : 'vim', 'd' : 'vim', 'pdf' : 'open', 'mp3' : 'open',
            \ 'jpg' : 'open', 'png' : 'open',
            \ }
let g:vimfiler_execute_file_list['_'] = 'vim'
let g:vimfiler_split_rule = 'botright'

" vimfiler.vim のキーマップ {{{
" smart s mapping for edit or cd
autocmd MyVimrc FileType vimfiler nmap <buffer><silent><expr> s vimfiler#smart_cursor_map(
            \ "\<Plug>(vimfiler_cd_file)",
            \ "\<Plug>(vimfiler_edit_file)")
" jump to VimShell
autocmd MyVimrc FileType vimfiler nnoremap <buffer><silent><Leader>vs
            \ :<C-u>VimShellCurrentDir<CR>
" e は元のまま使えるようにする
autocmd MyVimrc FileType vimfiler nmap <buffer>e <C-w>
" 'a'nother
autocmd MyVimrc FileType vimfiler nmap <buffer><silent>a <Plug>(vimfiler_switch_to_another_vimfiler)
" unite.vim に合わせる
autocmd MyVimrc FileType vimfiler nmap <buffer><silent><Tab> <Plug>(vimfiler_choose_action)
" <Space> の代わりに u を unite.vim のプレフィクスに使う
autocmd MyVimrc FileType vimfiler nmap <buffer><silent>u [unite]
" unite.vim の file_mru との連携
autocmd MyVimrc FileType vimfiler nnoremap <buffer><silent><C-h> :<C-u>Unite file_mru directory_mru<CR>
" unite.vim の file との連携
autocmd MyVimrc FileType vimfiler
            \ nnoremap <buffer><silent>/
            \ :<C-u>execute 'Unite' 'file:'.vimfiler#get_current_vimfiler().current_dir <CR>
" git リポジトリに登録されたすべてのファイルを開く
autocmd MyVimrc FileType vimfiler nnoremap <buffer><expr>ga vimfiler#do_action('git_repo_files')

nnoremap <Leader>f                <Nop>
nnoremap <Leader>ff               :<C-u>VimFiler<CR>
nnoremap <Leader>fs               :<C-u>VimFilerSplit<CR>
nnoremap <Leader><Leader>         :<C-u>VimFiler<CR>
nnoremap <Leader>fq               :<C-u>VimFiler -no-quit<CR>
nnoremap <Leader>fh               :<C-u>VimFiler ~<CR>
nnoremap <Leader>fc               :<C-u>VimFilerCurrentDir<CR>
nnoremap <Leader>fb               :<C-u>VimFilerBufferDir<CR>
" }}}
" }}}

" {{{ memolist
map <Leader>mn  :MemoNew<CR>
map <Leader>ml  :Unite file:<C-r>=g:memolist_path."/"<CR><CR>
nnoremap <silent>[unite]ml :Unite file:<C-r>=g:memolist_path."/"<CR><CR>
map <Leader>mg  :MemoGrep<CR>

let g:memolist_memo_suffix = "markdown"
let g:memolist_memo_date = "%Y-%m-%d %H:%M"
let g:memolist_prompt_tags = 1
let g:memolist_vimfiler = 1

let g:memolist_path = "~/Dropbox/Data/Memolist"

" }}}

" HTML {{{
autocmd MyVimrc FileType html setlocal tabstop=2 shiftwidth=2 softtabstop=2

" HTML 5 tags
syn keyword htmlTagName contained article aside audio bb canvas command
syn keyword htmlTagName contained datalist details dialog embed figure
syn keyword htmlTagName contained header hgroup keygen mark meter nav output
syn keyword htmlTagName contained progress time ruby rt rp section time
syn keyword htmlTagName contained source figcaption
syn keyword htmlArg contained autofocus autocomplete placeholder min max
syn keyword htmlArg contained contenteditable contextmenu draggable hidden
syn keyword htmlArg contained itemprop list sandbox subject spellcheck
syn keyword htmlArg contained novalidate seamless pattern formtarget
syn keyword htmlArg contained formaction formenctype formmethod
syn keyword htmlArg contained sizes scoped async reversed sandbox srcdoc
syn keyword htmlArg contained hidden role
syn match   htmlArg "\<\(aria-[\-a-zA-Z0-9_]\+\)=" contained
syn match   htmlArg contained "\s*data-[-a-zA-Z0-9_]\+"

" }}}

" CSS & SASS {{{

"let g:sass_compile_auto = 1
let g:sass_compile_cdloop = 5
let g:sass_compile_cssdir = ['css', 'stylesheet']
let g:sass_compile_file = ['scss', 'sass']
let g:sass_started_dirs = []
 
autocmd FileType less,sass  setlocal sw=2 sts=2 ts=2 et
"au! BufWritePost * SassCompi

" }}}

" PHP {{{
autocmd BufRead *.php\|*.ctp\|*.tpl :set dictionary=~/.vim/dict/php.dict filetype=php
autocmd MyVimrc FileType php setlocal tabstop=4 shiftwidth=4 softtabstop=4

let g:ref_phpmanual_path = $HOME . '/.dotfiles/doc/phpmanual'

" CakePHPのテンプレート
autocmd MyVimrc BufRead,BufNewFile *.ctp setlocal filetype=html shiftwidth=2 softtabstop=2

" Twigのテンプレート
autocmd MyVimrc BufRead,BufNewFile *.twig setlocal filetype=html shiftwidth=2 softtabstop=2

" }}}

" Ruby {{{
autocmd MyVimrc FileType ruby setlocal tabstop=2 shiftwidth=2 softtabstop=2

function! s:load_rspec_settings()
  nnoremap <buffer><Leader>cr  :<C-U>RSpecCurrent<CR>
  nnoremap <buffer><Leader>nr :<C-U>RSpecNearest<CR>
  nnoremap <buffer><Leader>lr :<C-U>RSpecRetry<CR>
  nnoremap <buffer><Leader>ar :<C-U>RSpecAll<CR>
  nnoremap <buffer><Leader>r :<C-U>RSpec<Space>
endfunction

augroup RSpecSetting
  autocmd!
  autocmd BufEnter *.rb call s:load_rspec_settings()
augroup END

" Ruby の guard 用ファイル
autocmd MyVimrc BufRead,BufNewFile Guardfile setlocal ft=ruby

" Ruby の guard 用ファイル
autocmd MyVimrc BufRead,BufNewFile Gemfile setlocal ft=ruby

autocmd MyVimrc BufRead,BufNewFile *.erb setlocal filetype=eruby shiftwidth=2 softtabstop=2

nnoremap rr :<C-U>Unite ref/refe     -default-action=split -input=
nnoremap ri :<C-U>Unite ref/ri       -default-action=split -input=

aug MyAutoCmd
  au FileType ruby,eruby,ruby.rspec nnoremap <silent><buffer>KK :<C-U>Unite -no-start-insert ref/ri   -input=<C-R><C-W><CR>
  au FileType ruby,eruby,ruby.rspec nnoremap <silent><buffer>K  :<C-U>Unite -no-start-insert ref/refe -input=<C-R><C-W><CR>
aug END

" }}}

" {{{ QuickRun

"<Leader>r を使わない
let g:quickrun_no_default_key_mappings = 1

if !has("g:quickrun_config")
    let g:quickrun_config = {}
endif
let g:quickrun_config.markdown = {
      \ 'outputter' : 'null',
      \ 'command'   : 'open',
      \ 'cmdopt'    : '-a',
      \ 'args'      : 'Marked',
      \ 'exec'      : '%c %o %a %s',
      \ }

" QuickRunのキーマップ {{{

nnoremap <Leader>q  <Nop>
nnoremap <silent><Leader>qr :<C-u>QuickRun<CR>
nnoremap <silent><Leader>qf :<C-u>QuickRun >quickfix -runner vimproc<CR>
vnoremap <silent><Leader>qr :QuickRun<CR>
vnoremap <silent><Leader>qf :QuickRun >quickfix -runner vimproc<CR>
nnoremap <silent><Leader>qR :<C-u>QuickRun<Space>

" }}}
"
" }}}

" BasicMapping {{{

nnoremap Y y$

noremap j gj
noremap k gk

" nnoremap <C-j> }
" nnoremap <C-k> {

" Emacs like mapping
inoremap <C-e> <END>
vnoremap <C-e> <END>
cnoremap <C-e> <END>
inoremap <C-a> <HOME>
vnoremap <C-a> <HOME>
cnoremap <C-a> <HOME>
inoremap <silent><expr><C-n> pumvisible() ? "\<C-y>\<Down>" : "\<Down>"
inoremap <silent><expr><C-p> pumvisible() ? "\<C-y>\<Up>" : "\<Up>"
inoremap <silent><expr><C-b> pumvisible() ? "\<C-y>\<Left>" : "\<Left>"
inoremap <silent><expr><C-f> pumvisible() ? "\<C-y>\<Right>" : "\<Right>"
cnoremap <C-f> <Right>
cnoremap <C-b> <Left>
inoremap <C-d> <Del>
cnoremap <C-d> <Del>
" inoremap <silent><expr><C-k> "\<C-g>u".(col('.') == col('$') ? '<C-o>gJ' : '<C-o>D')
" cnoremap <C-k> <C-\>e getcmdpos() == 1 ? '' : getcmdline()[:getcmdpos()-2]<CR>

" Tab
nnoremap te :<C-u>tabedit<Space>
nnoremap tc :<C-u>tabnew<CR>
nnoremap tn :<C-u>tabnext<CR>
nnoremap tp :<C-u>tabprevious<CR>
nnoremap <silent>tc :<C-u>tabclose<CR>

" }}}

" vim-precious {{{

" コンテキストが切り替わった場合に処理をフック
" ※ここに載っている例はわたしの環境の場合の設定です
augroup my-augroup
    autocmd!
    " vim のコンテキストに切り替わった時に
    " いくつかのマッピングを削除する
"    autocmd User PreciousFileTypeLeave_vim iunmap <buffer> <CR>
"    autocmd User PreciousFileTypeLeave_vim nunmap <buffer> <Leader><Leader>r
augroup END

" filetype=help は insert 時のみ切り替わるように設定
let g:precious_enable_switch_CursorMoved = {
\   "help" : 0
\}

let g:context_filetype#filetypes = {
            \ 'html': [
            \   {
            \    'start':
            \     '<script\%( [^>]*\)\? type="text/javascript"\%( [^>]*\)\?>',
            \    'end': '</script>', 'filetype': 'javascript',
            \   },
            \   {
            \    'start':
            \     '<script\%( [^>]*\)\? type="text/coffeescript"\%( [^>]*\)\?>',
            \    'end': '</script>', 'filetype': 'coffee',
            \   },
            \   {
            \    'start': '<style\%( [^>]*\)\? type="text/css"\%( [^>]*\)\?>',
            \    'end': '</style>', 'filetype': 'css',
            \   },
            \   {
            \    'start': '<?php\?',
            \    'end': '?>', 'filetype': 'php',
            \   }
            \ ],}

augroup test
    autocmd!
    autocmd FileType vim nnoremap <buffer> <leader><leader>r :so %<CR>
    " PreciousFileTypeLeave_vim は vim のコンテキストから抜ける時に呼ばれる
    " そのタイミングでマッピングを削除しておく
    autocmd User PreciousFileTypeLeave_vim nunmap <buffer> <Leader><Leader>r
augroup END
" }}}

" {{{ Kobito
function! s:open_kobito(...)
    if a:0 == 0
        call system('open -a Kobito '.expand('%:p'))
    else
        call system('open -a Kobito '.join(a:000, ' '))
    endif
endfunction

" 引数のファイル(複数指定可)を Kobitoで開く
" （引数無しのときはカレントバッファを開く
command! -nargs=* Kobito call s:open_kobito(<f-args>)
" Kobito を閉じる
command! -nargs=0 KobitoClose call system("osascript -e 'tell application \"Kobito\" to quit'")
" Kobito にフォーカスを移す
command! -nargs=0 KobitoFocus call system("osascript -e 'tell application \"Kobito\" to activate'")
" }}}

" vim-indent-guides {{{
let g:indent_guides_enable_on_vim_startup = 1 "vim-indent-guidesの自動有効化
let g:indent_guides_guide_size = 1 "インデントの色付け幅
let g:indent_guides_start_level = 1
let g:indent_guides_space_guides = 1
" }}}

" Syntax
syntax enable
