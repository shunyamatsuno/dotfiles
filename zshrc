# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="gentoo"

plugins=(git ruby vagrant osx pass rbenv capistrano cake knife node rsync rake sublime gem npm tmux)

export DISABLE_AUTO_UPDATE="true"

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=/Applications/Vagrant/embedded:/Applications/Vagrant/bin:$HOME/.nodebrew/current/bin:/usr/local/bin:/usr/local/sbin:~/.phpenv/bin:~/.dotfile/bin:/Applications/AndroidDeveloperTools/21/platform-tools:$HOME/.dotfiles/bin:$PATH:$HOME/.nodebrew/current/bin
export LANG=ja_JP.UTF-8
export GNUTERM=x11

# for tmux
# Reference: http://kazuph.hateblo.jp/entry/2013/05/06/200059
if [ -d ${HOME}/.rbenv ] ; then
    export PATH="${HOME}/.rbenv/bin:${HOME}/.rbenv/shims:${PATH}"
    eval "$(rbenv init -)"
fi

alias sl="ls"
alias la="ls -a"
alias lf="ls -F"
alias ll="ls -l"

alias du="du -h"
alias df="df -h"

alias vi='vim'
alias macvim="/Applications/MacVim.app/Contents/MacOS/Vim -g --remote-tab-silent"

alias subl="/Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl"

alias javac="javac -J-Dfile.encoding=UTF8"

compdef mosh=ssh

setopt print_eight_bit
